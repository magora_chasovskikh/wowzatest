//
//  FirstViewController.swift
//  WowzaTest
//
//  Created by Alexander Chasovskih on 27/07/2017.
//  Copyright © 2017 Magora Systems. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit
//import MediaPlayer

class WatchViewController: UIViewController {
    var playerViewController = AVPlayerViewController()
    let myPlayerViewController = VideoPlayerViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()

//        let url = URL.init(string: "https://eoimages.gsfc.nasa.gov/images/imagerecords/79000/79803/earth_night_rotate_web.h264.mov")
//        let url2 = URL.init(string: "https://devstreaming-cdn.apple.com/videos/streaming/examples/bipbop_4x3/bipbop_4x3_variant.m3u8")
//        let url3 = URL.init(string: "https://tungsten.aaplimg.com/VOD/bipbop_adv_example_hevc/master.m3u8")
        let url3 = URL.init(string: "https://596842.entrypoint.cloud.wowza.com/app-2adf/ngrp:ecc5006c_all/playlist.m3u8")
        
        let asset = AVURLAsset.init(url: url3!)
        let item = AVPlayerItem.init(asset: asset)
        let player = AVPlayer.init(playerItem: item)
        playerViewController.player = player
        playerViewController.view.frame = CGRect(x: 0, y: 0, width: 200, height: 200)
        self.view.addSubview((playerViewController.view)!)
        playerViewController.player?.play()
        
        myPlayerViewController.url = url3
        myPlayerViewController.view.frame = CGRect(x: 40, y: 250, width: 240, height: 160)
        self.view.addSubview((myPlayerViewController.view)!)
    }
}

