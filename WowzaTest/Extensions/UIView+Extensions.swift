//
//  UIView+Additions.swift
//  WowzaTest
//
//  Created by Alexander Chasovskih on 24/08/2017.
//  Copyright © 2017 Magora Systems. All rights reserved.
//

import UIKit

extension UIView {
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
}
