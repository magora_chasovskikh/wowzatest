//
//  Insets.swift
//  WowzaTest
//
//  Created by Alexander Chasovskih on 24/08/2017.
//  Copyright © 2017 Magora Systems. All rights reserved.
//

import UIKit

class LabelWithInsets: UILabel {
    @IBInspectable var insets = UIEdgeInsets.zero
    @IBInspectable var inset: CGFloat {
        get {
            return insets.left
        }
        set {
            insets = UIEdgeInsetsMake(newValue, newValue, newValue, newValue)
        }
    }
    
    override func drawText(in rect: CGRect) {
        super.drawText(in: UIEdgeInsetsInsetRect(rect, insets))
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
    }
    
    override var intrinsicContentSize: CGSize {
        get {
            var intrinsicSize = super.intrinsicContentSize
            intrinsicSize.width += (insets.left + insets.right)
            intrinsicSize.height += (insets.top + insets.bottom)
            return intrinsicSize
        }
    }
}
