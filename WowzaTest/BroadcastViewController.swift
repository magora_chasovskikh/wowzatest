//
//  SecondViewController.swift
//  WowzaTest
//
//  Created by Alexander Chasovskih on 27/07/2017.
//  Copyright © 2017 Magora Systems. All rights reserved.
//

import UIKit
import WowzaGoCoderSDK

class BroadcastViewController: UIViewController, WZStatusCallback {

    var goCoder: WowzaGoCoder?
    let appLicenseKey = "GOSK-FC43-0103-174D-7ABB-5520"
    
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var broadcastButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let goCoderLicensingError = WowzaGoCoder.registerLicenseKey(appLicenseKey) {
            print(goCoderLicensingError.localizedDescription)
            return
        }
        
        goCoder = WowzaGoCoder.sharedInstance()
        guard goCoder != nil else {
            return
        }
        
        goCoder?.cameraView = self.view
        goCoder?.cameraPreview?.start()
        
        let goCoderBroadcastConfig = goCoder?.config
        goCoderBroadcastConfig?.load(.preset1280x720)
        
        goCoderBroadcastConfig?.hostAddress = "596842.entrypoint.cloud.wowza.com"
        goCoderBroadcastConfig?.portNumber = 1935
        goCoderBroadcastConfig?.applicationName = "app-2adf"
        goCoderBroadcastConfig?.streamName = "f854d9b8"
        goCoderBroadcastConfig?.username = "client24077"
        goCoderBroadcastConfig?.password = "50d4af79"
        
        goCoder?.config = goCoderBroadcastConfig!
    }
    
    
    @IBAction func broadcastAction(_ sender: UIButton) {
        if let configValidationError = goCoder?.config.validateForBroadcast() {
            self.showAlert(configValidationError.localizedDescription)
            return
        }
        
        if goCoder?.status.state != .running {
            goCoder?.startStreaming(self)
        }
        else {
            goCoder?.endStreaming(self)
        }
    }

    func showAlert(_ message: String?) {
        let alertController = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .cancel) { action in
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    // MARK: - Wowza delegate
    
    func onWZStatus(_ status: WZStatus!) {
        print("Status: \(status)")
        self.statusLabel.text = "\(status)"
        
        switch status.state {
        case .idle:
            self.broadcastButton.setTitle("😒", for: .normal)
            self.statusLabel.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1).withAlphaComponent(0.15)
            self.statusLabel.text = "Idle"
            break
        case .starting:
            self.broadcastButton.setTitle("☺️", for: .normal)
            self.statusLabel.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1).withAlphaComponent(0.15)
            self.statusLabel.text = "Starting broadcast..."
            break
        case .running:
            self.broadcastButton.setTitle("😃", for: .normal)
            self.statusLabel.backgroundColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
            self.statusLabel.text = "ON AIR"
            break
        case .stopping:
            self.broadcastButton.setTitle("😬", for: .normal)
            self.statusLabel.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1).withAlphaComponent(0.15)
            self.statusLabel.text = "Stopping broadcast..."
            break
        case .buffering:
            self.broadcastButton.setTitle("🤢", for: .normal)
            self.statusLabel.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1).withAlphaComponent(0.15)
            self.statusLabel.text = "Buffering..."
            break
        case .ready:
            self.broadcastButton.setTitle("🙂", for: .normal)
            self.statusLabel.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1).withAlphaComponent(0.15)
            self.statusLabel.text = "Ready"
            break
        }
    }
    
    func onWZError(_ status: WZStatus!) {
        print("Error: \(status)")
        self.showAlert(status.error?.localizedDescription)
        self.statusLabel.text = "Idle"
    }
    
    func onWZEvent(_ status: WZStatus!) {
        print("Event: \(status)")
        switch status.event {
        case .none:
            
            break
        case .lowBandwidth:
            
            break
        case .bitrateReduced:
            
            break
        case .bitrateIncreased:
            
            break
        case .encoderPaused:
            
            break
        case .encoderResumed:
            
            break
        }
    }
}

