//
//  VideoPlayerView.swift
//  WowzaTest
//
//  Created by Alexander Chasovskih on 28/07/2017.
//  Copyright © 2017 Magora Systems. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit

class VideoPlayerView: UIView {
    var player: AVPlayer? {
        get {
            return playerLayer.player
        }
        set {
            playerLayer.player = newValue
        }
    }
    
    var playerLayer: AVPlayerLayer {
        return layer as! AVPlayerLayer
    }

    // Override UIView property
    override static var layerClass: AnyClass {
        return AVPlayerLayer.self
    }
    
    func setVideoFillMode(_ fillMode: String) {
        playerLayer.videoGravity = fillMode
    }
}
