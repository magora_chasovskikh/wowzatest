//
//  VideoPlayerViewController.swift
//  WowzaTest
//
//  Created by Alexander Chasovskih on 28/07/2017.
//  Copyright © 2017 Magora Systems. All rights reserved.
//

import UIKit
import AVFoundation

/* Asset keys */
let kTracksKey: String = "tracks"
let kPlayableKey: String = "playable"

class VideoPlayerViewController: UIViewController {
    private var AVPlayerDemoPlaybackViewControllerCurrentItemObservationContext = 0
    private var AVPlayerDemoPlaybackViewControllerStatusObservationContext = 0

    var player: AVPlayer?
    var playerItem: AVPlayerItem?
    let playerView = VideoPlayerView()

    var urlInternal: URL?
    var url: URL? {
        get {
            return self.urlInternal
        }
        set {
            self.urlInternal = newValue

            let asset = AVURLAsset.init(url: self.urlInternal!)
            let requestedKeys = [ kTracksKey, kPlayableKey ]
            
            asset.loadValuesAsynchronously(forKeys: requestedKeys) {
                DispatchQueue.main.async {
                    self.prepare(toPlay: asset, keys: requestedKeys)
                }
            }
        }
    }
    
    override func loadView() {
        super.loadView()
        
        self.view = playerView
    }
    
    private func prepare(toPlay asset: AVURLAsset, keys: Array<String>) {
        for key in keys {
            var error: NSError? = nil
            guard asset.statusOfValue(forKey: key, error: &error) == .loaded
            else { print(error ?? "Unknown error"); return }
        }
        
        guard asset.isPlayable else {
            return;
        }
        
        if playerItem != nil {
            playerItem?.removeObserver(self, forKeyPath: #keyPath(playerItem.status))
            NotificationCenter.default.removeObserver(self, name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: playerItem)
        }
        
        playerItem?.addObserver(self, forKeyPath: #keyPath(playerItem.status), options: [.initial, .new], context: &AVPlayerDemoPlaybackViewControllerStatusObservationContext)
        playerItem = AVPlayerItem(asset: asset)
        
        if self.player == nil {
            self.player = AVPlayer.init(playerItem: playerItem)
            self.player?.addObserver(self, forKeyPath: #keyPath(player.currentItem), options: [.initial, .new], context: &AVPlayerDemoPlaybackViewControllerCurrentItemObservationContext)
        }
        
        if self.player?.currentItem != self.playerItem {
            self.player?.replaceCurrentItem(with: self.playerItem)
        }
        self.player?.play()
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if context == &AVPlayerDemoPlaybackViewControllerStatusObservationContext {
            let status : AVPlayerStatus? = change?[.newKey] as? AVPlayerStatus
            if status == .readyToPlay {
                self.player?.play()
            }
        }
        else if context == &AVPlayerDemoPlaybackViewControllerCurrentItemObservationContext {
            if change?[.newKey] as? AVPlayerItem != nil {
                self.playerView.player = self.player
                self.playerView.setVideoFillMode(AVLayerVideoGravityResizeAspect)
            }
        }
        else {
            super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
        }
    }
    
    deinit {
        player?.removeObserver(self, forKeyPath: #keyPath(player.currentItem))
        playerItem?.removeObserver(self, forKeyPath: #keyPath(playerItem.status))
    }
}
